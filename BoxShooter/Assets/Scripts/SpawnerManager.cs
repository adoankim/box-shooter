﻿using UnityEngine;
using System.Collections;

public class SpawnerManager : MonoBehaviour {
	
	public float spawnerSwapTime = 10f;
	public GameObject [] Spawners;
	private float nextSwapTime;
	private int currentSwap = 0;

	void Start () {
		
		nextSwapTime = Time.time+spawnerSwapTime;
		Spawners [currentSwap].SetActive (true);
	}


	void Update () {
		if (!GameManager.gm.gameIsOver) {
			if (Time.time  >= nextSwapTime) {
				swapSpawners();
				nextSwapTime = Time.time+spawnerSwapTime;
			}
		}
	}
	
	void swapSpawners ()
	{
			Spawners [currentSwap].SetActive (false);
			currentSwap = (currentSwap + 1) % Spawners.Length;
			Spawners [currentSwap].SetActive (true);

	}
}
